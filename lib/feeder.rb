module Feeder
  class Parser
    @@nl = "\n"
    @@sd_photos = [
      'http://www.imageup.ru/img118/2646263/sd-1.jpg',
      'http://www.imageup.ru/img118/2646265/sd-2.jpg',
      'http://www.imageup.ru/img118/2646267/sd-3.jpg',
      'http://www.imageup.ru/img118/2646268/sd-4.jpg',
      'http://www.imageup.ru/img118/2646270/sd-5.jpg',
      'http://www.imageup.ru/img118/2646271/sd-6.jpg'
    ]
    
    @@y_photos = [
      'http://www.imageup.ru/img118/2646274/yuntolovo-1.jpg',
      'http://www.imageup.ru/img118/2646277/yuntolovo-2.jpg',
      'http://www.imageup.ru/img118/2646278/yuntolovo-3.jpg'
    ]
    
    @@no_y_addr = []
    
    @@sev_addresses = 
      {
        queue: 
        {
          1 => {
            build: {
              2 => "Санкт-Петербург, пос. Парголово, ул Николая Рубцова, д.11, к 1",
              '2_id' => 42481, '2_d' => 11, '2_kor' => 1,
              5 => "Санкт-Петербург, пос. Парголово, ул Михаила Дудина, д.23, к 1",
              '5_id' => 27530, '5_d' => 23, '5_kor' => 1,
              8 => "Санкт-Петербург, пос. Парголово, ул Михаила Дудина, д.25, к 1",
              '8_id' => 27644, '8_d' => 25, '8_kor' => 1,
              9 => "Санкт-Петербург, пос. Парголово, ул Михаила Дудина, д.2, к 2",
               '9_d' => 2, '9_kor' => 2,
              12 => "Санкт-Петербург, пос. Парголово, ул Николая Рубцова, д.9, литера А",
              '12_id' => 42481, '12_d' => 9, '12_lit' => 'А',
            }
          },
          2 => {
            build: {
              1 => "Санкт-Петербург, пос. Парголово, ул Федора Абрамова, д.4",
              '1_d' => 4,
              9 => "Санкт-Петербург, пос. Парголово, ул Федора Абрамова, д.20, к 1",
              '9_id' => 42482, '9_d' => 20, '9_kor' => 1,
              10 => "Санкт-Петербург, пос. Парголово, ул Федора Абрамова, д.18, к 1",
              '10_id' => 42479, '10_d' => 18, '10_kor' => 1,
              11 => "Санкт-Петербург, пос. Парголово, ул Федора Абрамова, д.16, к 1",
              '11_id' => 42483, '11_d' => 16, '11_kor' => 1,
              17 => "Санкт-Петербург, поселок Парголово, пр Энгельса, уч 20 к 17",
              '17_id' => 36293, '17_kor' => 17
            }
          },
          3 => {
            build: {
              6 => "Санкт-Петербург,  пос. Парголово, ул Валерия Гаврилина, дом 5",
              '6_id' => 27529, '6_d' => 5,
              7 => "Санкт-Петербург,  пос. Парголово, ул Заречная, дом 35, к 1",
              '7_id' => 27598, '7_d' => 35, '7_kor' => 1,
              8 => "Санкт-Петербург,  пос. Парголово, ул Заречная, дом 37, литера А",
              '8_id' => 27645, '8_d' => 37, '8_lit' => 'А',
              10 => "Санкт-Петербург, пос. Парголово, ул. Заречная, дом 25",
              '10_id' => 27530, '10_d' => 25,
              11 => "Санкт-Петербург, поселок Парголово, ул Валерия Гаврилина, дом 3, к 1",
              '11_id' => 38685, '11_d' => 3, '11_kor' => 1,
              12 => "Санкт-Петербург, пос. Парголово, ул Валерия Гаврилина, дом 3, к 1",
              '12_id' => 38686, '12_d' => 3, '12_kor' => 1,
              13 => "Санкт-Петербург, пос. Парголово, ул Валерия Гаврилина, дом 3, к 1",
              '13_id' => 38687, '13_d' => 3, '13_kor' => 1,
              14 =>"Санкт-Петербург, пос. Парголово, ул Валерия Гаврилина, дом 3, к 2",
              '14_id' => 38689, '14_d' => 3, '14_kor' => 2,
              15 => "Санкт-Петербург, пос. Парголово, ул Валерия Гаврилина, дом 3, к 2",
              '15_id' => 38688, '15_d' => 3, '15_kor' => 2
            }
          },
          4 => {
            build: {
              1 => "Санкт-Петербург, пос. Парголово, ул Федора Абрамова, дом 13",
              '1_id' => 26471, '1_d' => 13, '1_lit' => 'А',
              2 => "Санкт-Петербург, пос. Парголово, ул Федора Абрамова, дом 8",
              '2_id' => 38690, '2_d' => 8,
              3 => "Санкт-Петербург, пос. Парголово, ул Федора Абрамова, дом 8",
              '3_id' => 38691, '3_d' => 8,
              4 => "Санкт-Петербург, пос. Парголово, ул Федора Абрамова, дом 8",
              '4_id' => 38692, '4_d' => 8
            }
          },
          5 => {
            build: {
              2 => "Санкт-Петербург, пос. Парголово, ул Николая Рубцова, дом 11, к 1",
              '2_id' => 42481, '2_d' => 11, '2_kor' => 1,
              12 => "Санкт-Петербург, пос. Парголово, ул Николая Рубцова, дом 9",
              '12_id' => 42480, '12_d' => 9
            }
          },
          6 => {
            build: {
              5 => "Санкт-Петербург, пос. Парголово, ул Михаила Дудина, дом 23, к 1",
              '5_id' => 27530, '5_d' => 23, '5_kor' => 1, 
              6 => "Санкт-Петербург, пос. Парголово, ул Николая Рубцова, уч 6, к 6",
              '6_id' => 27442, '6_kor' => 6, 
              8 => "Санкт-Петербург, пос. Парголово, ул Михаила Дудина, дом 25, к 1",
              '8_id' => 27644, '8_d' => 25, '8_kor' => 1, 
              9 => "Санкт-Петербург, пос. Парголово, ул Федора Абрамова, дом 20, к 1",
              '9_id' => 42482, '9_d' => 20, '9_kor' => 1, 
              10 => "Санкт-Петербург, пос. Парголово, ул Федора Абрамова, дом 18, к 1",
              '10_id' => 42479, '10_d' => 18, '10_kor' => 1,
              11 => "Санкт-Петербург, пос. Парголово, ул Федора Абрамова, дом 16, к 1",
              '11_id' => 42483, '11_d' => 16, '11_kor' => 1
            }
          },
          7 => {
            build: {
              5 => "Санкт-Петербург, пос. Парголово, ул Фёдора Абрамова, дом 15, литера А",
              '5_id' => 27441, '5_d' => 15, '5_lit' => 'А',
              6 => "Санкт-Петербург, пос. Парголово, ул Валерия Гаврилина, дом 5, литера А",
              '6_id' => 27529, '6_d' => 5, '6_lit' => 'А',
              7 => "Санкт-Петербург, пос. Парголово, ул Фёдора Абрамова, дом 21, к 1, литера А",
               '7_d' => 21, '7_kor' => 1, '7_lit' => 'А',
              8 => "Санкт-Петербург, пос. Парголово, ул Фёдора Абрамова, дом 23, к 1, литера А",
               '8_d' => 23, '8_kor' => 1, '8_lit' => 'А'
            }
          },
          8 => {
            build: {
              1 => "Санкт-Петербург, пос. Парголово, ул Николая Рубцова, дом 13, литера А",
              '1_id' => 26471, '1_d' => 13, '1_lit' => 'А',
              7 => "Санкт-Петербург, пос. Парголово, ул Заречная, дом 35, к 1, литера А",
              '7_id' => 27598, '7_d' => 35, '7_kor' => 1, '7_lit' => 'А',
              8 => "Санкт-Петербург, пос. Парголово, ул Заречная, дом 37, литера А",
              '8_id' => 27645, '8_d' => 37, '8_lit' => 'А'
            }
          },
          9 => {
            build: {
              3 => "Санкт-Петербург, поселок Парголово, ул Валерия Гаврилина к 3",
              '3_id' => 36294, '3_kor' => 3,
              4 => "Санкт-Петербург, поселок Парголово, ул Валерия Гаврилина к 4",
              '4_id' => 36295, '4_kor' => 4,
              5 => "Санкт-Петербург, поселок Парголово, ул Николая Рубцова, уч 5 к 5",
               '5_kor' => 5,
              6 => "Санкт-Петербург, поселок Парголово, ул Николая Рубцова, уч 6 к 6",
              '6_id' => 27442, '6_kor' => 6,
              7 => "Санкт-Петербург, поселок Парголово, ул Николая Рубцова, дом 12, к 1",
              '7_id' => 27597, '7_d' => 12, '7_kor' => 1,
            }
          },
          10 => {
            build: {
              17 => "Санкт-Петербург, пос. Парголово, пр Энгельса, уч 20 к 17",
              '17_id' => 36293, '17_kor' => 17,
            }
          },
          11 => {
            build: {
              3 => "Санкт-Петербург, поселок Парголово, пр Энгельса, уч 20 к 3,4",
              '3_id' => 43425, '3_kor' => 3,
              4 => "Санкт-Петербург, пос. Парголово, пр Энгельса, уч 20 к 4",
              '4_id' => 43426, '4_kor' => 4,
              9 => "Санкт-Петербург, пос. Парголово, пр Энгельса, уч 20 к 9",
               '9_kor' => 9,
              11 => "Санкт-Петербург, пос. Парголово, ул Валерия Гаврилина, дом 3, к 1",
              '11_id' => 38685, '11_kor' => 1, '11_d' => 3,
              12 => "Санкт-Петербург, пос. Парголово, ул Валерия Гаврилина, дом 3, к 1",
              '12_id' => 38686, '12_kor' => 1, '12_d' => 3,
              13 => "Санкт-Петербург, пос. Парголово, ул Валерия Гаврилина, дом 3, к 1",
              '13_id' => 38687, '13_kor' => 1, '13_d' => 3,
              14 => "Санкт-Петербург, пос. Парголово, ул Валерия Гаврилина, дом 3, к 2",
              '14_id' => 38688, '14_kor' => 2, '14_d' => 3,
              15 => "Санкт-Петербург, пос. Парголово, ул Валерия Гаврилина, дом 3, к 2",
              '15_id' => 38689, '15_kor' => 2, '15_d' => 3,
              16 => "Санкт-Петербург, пос. Парголово, пр Энгельса, уч 20 к 16",
               '16_kor' => 16
            }
          }
        }
      }

    @@yunt_addresses = 
      {
        queue: {
          0 => {
            build: {
              3 => "Санкт-Петербург, Юнтоловский пр, д.53 к 3",
              '3_id' => 27125,
              4 => "Санкт-Петербург, Юнтоловский пр, д.51 к 3",
              '4_id' => 31253,
              5 => "Санкт-Петербург, Юнтоловский пр, д.51 к 4",
              '5_id' => 27409,
              6 => "Санкт-Петербург, Юнтоловский пр, д.53 к 4",
              '6_id' => 31261,
              7 => "Санкт-Петербург, Юнтоловский пр, д.49 к 7",
              '7_id' => 31264,
              8 => "Санкт-Петербург, Юнтоловский пр, д.49 к 6",
              '8_id' => 31266,
              9 => "Санкт-Петербург, Юнтоловский пр, д.49 к 5",
              '9_id' => 31270,
              10 => "Санкт-Петербург, Юнтоловский пр, д.49 к 3",
              '10_id' => 31207,
              11 => "Санкт-Петербург, Юнтоловский пр, д.49 к 4",
              '11_id' => 31209,
              19 => "Санкт-Петербург, Юнтоловский пр, д.47 к 3",
              '19_id' => 31222,
              20 => "Санкт-Петербург, Юнтоловский пр, д.47 к 2",
              '20_id' => 31231,
              24 => "Санкт-Петербург, Юнтоловский пр, д.47 к 5",
              '24_id' => 31234,
              25 => "Санкт-Петербург, Юнтоловский пр, д.47 к 6",
              '25_id' => 31236,
              26 => "Санкт-Петербург, Юнтоловский пр, д.47 к 4",
              '26_id' => 31238,
              1 => "г. Санкт-Петербург, 3-я Конная Лахта, уч 2",
              '1_id' => 31198,
              2 => "г. Санкт-Петербург, 3-я Конная Лахта, уч 2",
              '2_id' => 31227,
              12 => "г. Санкт-Петербург, 3-я Конная Лахта, уч 2",
              '12_id' => 31210,
              13 => "г. Санкт-Петербург, 3-я Конная Лахта, уч 2",
              '13_id' => 31211,
              14 => "г. Санкт-Петербург, 3-я Конная Лахта, уч 2",
              '14_id' => 31215,
              18 => "г. Санкт-Петербург, 3-я Конная Лахта, уч 2",
              '18_id' => 31221,
              21 => "Санкт-Петербург, 3-я Конная Лахта, уч 3 к 21",
              '21_id' => 35926,
              22 => "Санкт-Петербург, 3-я Конная Лахта, уч 3 к 22",
              '22_id' => 42431,
              23 => "Санкт-Петербург, 3-я Конная Лахта, уч 3 к 23",
              '23_id' => 42432,
              27 => "Санкт-Петербург, 3-я Конная Лахта, уч 3 к 27",
              '27_id' => 42433,
              28 => "Санкт-Петербург, 3-я Конная Лахта, уч 3 к 28",
              '28_id' => 42434
            }
          }
        }
      }      

    
    def initialize(options)
      @ip = options[:ip]
      @imgs = Marshal.load File.read("public/imgs.txt")
    end

    def read_imgs
      sev = Marshal.load File.read('public/sevdol_tmp.txt') 
      yun = Marshal.load File.read('public/yuntolovo_tmp.txt') 
      offers = sev + yun
      @imgs = {}
      offers.each do |offer|
        if offer[:img] && !offer[:img].blank?
          @imgs[offer['Id']] = offer[:img]
          ap @imgs[offer['Id']]
        else  
          @imgs[offer['Id']] = get_image_url(offer[:url])
          ap @imgs[offer['Id']]
        end  
      end
      File.open("public/imgs.txt", 'w') { |file|  file.write Marshal.dump(@imgs) }
    end

    def reload_imgs
      sev = Marshal.load File.read('public/sevdol_tmp.txt') 
      yun = Marshal.load File.read('public/yuntolovo_tmp.txt') 
      offers = sev + yun
      @imgs = {}
      offers.each do |offer|
        @imgs[offer['Id']] = get_image_url(offer[:url])
        ap @imgs[offer['Id']]
      end
      File.open("public/imgs.txt", 'w') { |file|  file.write Marshal.dump(@imgs) }
    end
    
    def sev_addr queue, build
      @@sev_addresses[:queue][queue.to_i][:build][build.to_i] if @@sev_addresses[:queue][queue.to_i]
    end
    
    def sev_addr_id queue, build
      @@sev_addresses[:queue][queue.to_i][:build]["#{build.to_i}_id"] if @@sev_addresses[:queue][queue.to_i]
    end
    
    def sev_addr_d queue, build
      @@sev_addresses[:queue][queue.to_i][:build]["#{build.to_i}_d"] if @@sev_addresses[:queue][queue.to_i]
    end
    
    def sev_addr_kor queue, build
      @@sev_addresses[:queue][queue.to_i][:build]["#{build.to_i}_kor"] if @@sev_addresses[:queue][queue.to_i]
    end
    
    def sev_addr_lit queue, build
      @@sev_addresses[:queue][queue.to_i][:build]["#{build.to_i}_lit"] if @@sev_addresses[:queue][queue.to_i]
    end
    
    def yunt_addr_id queue, build
      @@yunt_addresses[:queue][0][:build]["#{build.to_i}_id"] #if @@yunt_addresses[:queue][queue.to_i]
    end
    
    def yunt_addr queue, build
#      if @@yunt_addresses[:queue][queue.to_i]
        @@yunt_addresses[:queue][0][:build][build.to_i] 
#      else
#        @@no_y_addr << ["Очередь: #{queue.to_i}", "Корпус #{queue.to_i}"]
#        nil
#      end  
    end
    
    def studia? offer
      offer['Type'] == '1 st'
    end
    
    def sevd? offer
      offer['Id'][0] == "s"
    end
    
    def yunt? offer
      offer['Id'][0] == "y"
    end
    
    def perform_all
#      perform_panorama
      perform_yuntolovo
      perform_sevdol
      perform_novostrojki
      perform_spbguru
      perform_emls
      perform_bn
      perform_avito
      

#      panorama = get_file 'panorama_ok.xml'
#      panorama[panorama.index('<generation-date>')+1..panorama.index('</generation-date>')+18] = ''
      yunt = get_file 'yuntolovo_ok.xml'
      yunt[yunt.index('<generation-date>')+1..yunt.index('</generation-date>')+18] = ''
      sevdol = get_file 'sevdol_ok.xml'
      sevdol[sevdol.index('<generation-date>')+1..sevdol.index('</generation-date>')+18] = ''
      all = yunt + sevdol
      all = all
        .gsub('<?xml version="1.0" encoding="utf-8"?>', ' ')
        .gsub('<realty-feed xmlns="http://webmaster.yandex.ru/schemas/feed/realty/2010-06">', ' ')
        .gsub('</realty-feed>', ' ')
        
      File.open("public/all_ok_tmp.xml", 'w') do |file| 
        file.puts '<?xml version="1.0" encoding="utf-8"?>'
        file.puts '<realty-feed xmlns="http://webmaster.yandex.ru/schemas/feed/realty/2010-06">'
        file.puts "<generation-date>#{Time.now.to_datetime}</generation-date>"
        file.puts all
        file.puts '</realty-feed>'        
      end
      File.rename("public/all_ok_tmp.xml", "public/all_ok_sber.xml")

      all_f = get_file 'all_ok_sber.xml'
      all_f = all_f.gsub(/<building-section-id>\w+<\/building-section-id>/,"")
      
      File.open("public/all_ok.xml", 'w') { |file| file.puts all_f } 
#      IO.copy_stream('public/all_ok.xml', 'public/bn_2.xml')
      
      all_f = all_f.gsub("245-37-59","385-01-08")
      all_f = all_f.gsub("385-27-78","385-66-26")
      File.open("public/yandex.xml", 'w') { |file| file.puts all_f } 
      
      write_log ['ALL ОК +++++++++++++++++']
     
      perform_mailru
      perform_mtk
      perform_domofond
      perform_zipal
      perform_marketcall
      perform_bsn
      perform_mirkvartir
      perform_gdeetotdom
      write_log ['STACK ОК %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'] 
      
    rescue => error
      write_log ['ERROR ALL================', error]    
    end
    
#    def perform_panorama
#      response = RestClient.get 'http://panorama360spb.ru/upload/apartaments/panorama_CRM.xml'
#      parsed = Crack::XML.parse(responperform_bsnse)['Projects']['Project']['Queues']['Queue']['Buildings']
#      reparsed = flatten_panorama parsed
#      File.open("public/panorama_tmp.txt", 'w') { |file|  file.write Marshal.dump(reparsed) }
#      write_xml_panorama reparsed
#      File.open("public/log.txt", 'a') do |file|
#        file.puts "#{Time.now + 3.hours} #{@ip} PANORAMA ОК +++++++++++++++++"
#      end
#    rescue => error
#      File.open("public/log.txt", 'a') do |file|
#        file.puts "#{Time.now + 3.hours} #{@ip} ERROR PANORAMA================"  
#        file.puts errorhttp://www.gearbest.com/smart-light-bulb/pp_230349.html
#        file.puts "=============================="
#      end
#    end

    def perform_yuntolovo
      response = RestClient.get 'http://yuntolovo-spb.ru/upload/apartaments/yuntolovo_CRM.xml'
      parsed = Crack::XML.parse(response)['Projects']['Project']['Queues']['Queue']
      reparsed = flatten_yunt parsed
      File.open("public/yuntolovo_tmp.txt", 'w') { |file|  file.write Marshal.dump(reparsed) }
      write_xml_yuntolovo reparsed
      write_log ['YUNTOLOVO ОК +++++++++++++++++']
    rescue => error
      write_log ['ERROR YUNTOLOVO================', error]     
    end

    def perform_sevdol
      response = RestClient.get 'http://sevdol.ru/upload/apartaments/sevdol_CRM.xml'
      parsed = Crack::XML.parse(response)['Projects']['Project']['Queues']['Queue']
#      File.open("public/resp.json", 'w') { |file| file.write Crack::XML.parse(response).to_json}
      reparsed = flatten_sevdol parsed
      File.open("public/sevdol_tmp.txt", 'w') { |file|  file.write Marshal.dump(reparsed) }
      write_xml_sevdol reparsed
      write_log ['SEVDOL ОК +++++++++++++++++']   
    rescue => error
      write_log ['ERROR SEVDOL ================', error]      
    end
    
    def perform_novostrojki
      sev = Marshal.load File.read('public/sevdol_tmp.txt') 
      yun = Marshal.load File.read('public/yuntolovo_tmp.txt') 
      offers = sev + yun
      
      write_xml_novostrojki offers
      write_log ['NOVOSTROJKI ОК +++++++++++++++++']   
    rescue => error
      write_log ['ERROR NOVOSTROJKI ================', error]      
    end
    
    def perform_spbguru
      sev = Marshal.load File.read('public/sevdol_tmp.txt') 
      yun = Marshal.load File.read('public/yuntolovo_tmp.txt') 
      
      write_xml_spbguru [sev, yun]
      write_log ['SPBGURU ОК +++++++++++++++++']   
    rescue => error
      write_log ['ERROR SPBGURU ================', error]      
    end
    
    def sev_test
      p url = "http://sevdol.ru/planirovki-i-ceny/11-ochered-14-korpus/10-floor/1-section/65/"
#      doc = Nokogiri::HTML(open(url, 
#          "User-Agent" => "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7",
#          "Host" => "sevdol.ru", "Connection" => "keep-alive", "Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" )) 
      doc = Nokogiri::HTML(Net::HTTP.get(URI.parse(url)).to_s) 

      ap doc
      img = doc.css(".block_flat_info .item .block_img img")[0]
      ap img
#      ap "==============="
      img['src']
    rescue => e
      ap e
      false
    end
    
    def test_stud
      sev = Marshal.load File.read('public/sevdol_tmp.txt') 
      sev = sev.find_all { |s| s['Tsquare'].to_f - s['Lsquare'].to_f < 20 }
      ap sev
    end
    
    def perform_mailru
      sev = Marshal.load File.read('public/sevdol_tmp.txt') 
     
      write_xml_mailru sev
      write_xml_mailru_nb sev
      
      write_log ['MAILRU ОК +++++++++++++++++'] 
    rescue => error
      write_log ['MAILRU ERROR ================', error]                
    end
    
    def perform_emls #CIAN
      sev = Marshal.load File.read('public/sevdol_tmp.txt') 
      yun = Marshal.load File.read('public/yuntolovo_tmp.txt') 
      offers = sev + yun
      
      write_xml_emls offers
      write_log ['EMLS ОК +++++++++++++++++'] 
    rescue => error
      write_log ['ERROR EMLS ================', error]                
    end
    
    def perform_bn
      sev = Marshal.load File.read('public/sevdol_tmp.txt') 
      yun = Marshal.load File.read('public/yuntolovo_tmp.txt') 
      
#      sev_st = sev.find_all { |offer| offer['Type'].to_i == 1 && (offer['Tsquare'].to_f - offer['Lsquare'].to_f < 20) }
#      sev_1 = sev.find_all { |offer| offer['Type'].to_i == 1 && (offer['Tsquare'].to_f - offer['Lsquare'].to_f >= 20) }
#      sev_2 = sev.find_all { |offer| offer['Type'].to_i == 2 }
#      sev_3 = sev.find_all { |offer| offer['Type'].to_i == 3 }
#      sev = (sev_st + sev_1 + sev_2 + sev_3)
#      
#      yun_st = yun.find_all { |offer| offer['Type'].to_i == 1 && (offer['Tsquare'].to_f - offer['Lsquare'].to_f < 20) }
#      yun_1 = yun.find_all { |offer| offer['Type'].to_i == 1 && (offer['Tsquare'].to_f - offer['Lsquare'].to_f >= 20) }
#      yun_2 = yun.find_all { |offer| offer['Type'].to_i == 2 }
#      yun_3 = yun.find_all { |offer| offer['Type'].to_i == 3 }
#      yun = (yun_st + yun_1 + yun_2 + yun_3)
      
      offers = sev + yun
      
      write_xml_bn offers
      write_log ['BN ОК +++++++++++++++++']    
    rescue => error
      write_log ['ERROR BN ================', error]      
    end
    
    def perform_avito
     
      td_s = 61
      td_y = 60

      sev = Marshal.load File.read('public/sevdol_tmp.txt') 
      yun = Marshal.load File.read('public/yuntolovo_tmp.txt') 
#      offers = sev.uniq { |s| [ s['Tsquare'], s['Dvalue'] ]}.take(100) + yun.uniq { |s| [ s['Tsquare'], s['Dvalue'] ] }.take(100)
      sev_no_show = [[1,9], [2,1], [3,10], [7,6], [7,7], [7,8], [9,5], 
        [10,17], [11,9], [11,13], [11,14], [11,15], [11,16]]

      sev.reject! { |offer| sev_no_show.include? [offer[:queue], offer[:build]] }
      
      sev_st = sev.find_all { |offer| offer['Type'].to_i == 1 && studia?(offer) }.take(td_s + 220) #+4
      sev_1 = sev.find_all { |offer| offer['Type'].to_i == 1 && !studia?(offer) }.take(2*td_s + 220) #+4
      sev_2 = sev.find_all { |offer| offer['Type'].to_i == 2 }.take(2*td_s + 220) #+4
      sev_3 = sev.find_all { |offer| offer['Type'].to_i == 3 }.take(td_s + 2000) #+3
      sev = sev_st + sev_1 + sev_2 + sev_3
      p 'sev'      
      p "s #{sev_st.length}"
      p "1 #{sev_1.length}"
      p "2 #{sev_2.length}"
      p "3 #{sev_3.length}"
      
      yun_st = yun.find_all { |offer| offer['Type'].to_i == 1 && studia?(offer) }.take(td_y + 20) 
      yun_1 = yun.find_all { |offer| offer['Type'].to_i == 1 && !studia?(offer) }.take(td_y + 520) #+11
      yun_2 = yun.find_all { |offer| offer['Type'].to_i == 2 }.take(td_y + 10)
      yun_3 = yun.find_all { |offer| offer['Type'].to_i == 3 }.take(td_y + 10)
      yun = yun_st + yun_1 + yun_2 + yun_3
      p 'yun'      
      p "s #{yun_st.length}"
      p "1 #{yun_1.length}"
      p "2 #{yun_2.length}"
      p "3 #{yun_3.length}"
      p 'offers'
      offers = sev + yun
      p offers.length
      write_xml_avito offers
      write_log ["AVITO ОК +++++++++++++++++   Ю:#{yun.length}  С:#{sev.length} "]
      write_log ["Без адресов (Ю) #{@@no_y_addr.uniq}"]  
    rescue => error
      write_log ['ERROR AVITO ================', error]
    end
    
    def perform_domofond
#      all = get_file "all_ok.xml"
#      all = all.gsub("245-37-59","245-34-89")
#      all = all.gsub("385-27-78","385-27-44")
#      File.open("public/all_ok_domo.xml", 'w') do |file|
#        file.puts all
#      end 
      
      sev = Marshal.load File.read('public/sevdol_tmp.txt') 
      yun = Marshal.load File.read('public/yuntolovo_tmp.txt') 
      offers = sev + yun
      write_xml_domofond offers
      write_log ['DOMOFOND ОК +++++++++++++++++']
    rescue => error
      write_log ['DOMOFOND ERROR ================', error]    
    end

     def perform_zipal
      all = get_file "all_ok.xml"
      all = all.gsub("245-37-59","429-67-79")
      all = all.gsub("385-27-78","429-68-23")
      all = all.gsub("<address>Санкт-Петербург,","<address>")
      File.open("public/all_ok_zipal.xml", 'w') do |file|
        file.puts all
      end 
      write_log ['ZIPAL ОК +++++++++++++++++']      
    rescue => error
      write_log ['ZIPAL ERROR ================', error]
    end   

     def perform_marketcall
      all = get_file "all_ok.xml"
      all = all.gsub("245-37-59","643-15-86")
      all = all.gsub("385-27-78","385-06-79")
      File.open("public/all_ok_marketcall.xml", 'w') do |file|
        file.puts all
      end 
      write_log ['MARKETCALL ОК +++++++++++++++++']      
    rescue => error
      write_log ['MARKETCALL ERROR ================', error]     
    end   

    def perform_bsn
      all = get_file "all_ok.xml"
      all = all.gsub("245-37-59","245-37-59")
      all = all.gsub("385-27-78","385-27-78")
      File.open("public/bsn.xml", 'w') do |file|
        file.puts all
      end 
      write_log ['BSN ОК +++++++++++++++++']  
    rescue => error
      write_log ['BSN ERROR ================', error]      
    end   

    def perform_mtk
      sev = Marshal.load File.read('public/sevdol_tmp.txt') 
      yun = Marshal.load File.read('public/yuntolovo_tmp.txt') 
      sev = sev.take(500)
      yun = yun.take(499)
      write_xml_yuntolovo yun, 'y_mtk.xml'
      write_xml_sevdol sev, 's_mtk.xml'

      yunt = get_file 'y_mtk.xml'
      yunt[yunt.index('<generation-date>')+1..yunt.index('</generation-date>')+18] = ''
      sevdol = get_file 's_mtk.xml'
      sevdol[sevdol.index('<generation-date>')+1..sevdol.index('</generation-date>')+18] = ''
      all = yunt + sevdol
      all = all
        .gsub('<?xml version="1.0" encoding="utf-8"?>', ' ')
        .gsub('<realty-feed xmlns="http://webmaster.yandex.ru/schemas/feed/realty/2010-06">', ' ')
        .gsub('</realty-feed>', ' ')
        .gsub(/<building-section-id>\w+<\/building-section-id>/,"")
        .gsub("<balcony>1</balcony>","<balcony>балкон</balcony>")
        .gsub("+7 (812) 385-27-78","+7 (812) 413-90-89")
        .gsub("+7 (812) 245-37-59","+7 (812) 413-90-89")
              
      File.open("public/mtk_tmp.xml", 'w') do |file| 
        file.puts '<?xml version="1.0" encoding="utf-8"?>'
        file.puts '<realty-feed xmlns="http://webmaster.yandex.ru/schemas/feed/realty/2010-06">'
        file.puts "<generation-date>#{Time.now.to_datetime}</generation-date>"
        file.puts all
        file.puts '</realty-feed>'        
      end
      File.rename("public/mtk_tmp.xml", "public/mtk.xml")
      write_log ['MTK ОК +++++++++++++++++']  
    rescue => error
      write_log ['MTK ERROR ================', error]      
    end   

    def perform_mirkvartir
      yunt = get_file 'yuntolovo_ok.xml'
      sevdol = get_file 'sevdol_ok.xml'
      
      sevdol = sevdol.gsub("245-37-59","245-37-59")
      yunt = yunt.gsub("385-27-78","385-27-78")
      
      sevdol = sevdol.gsub(', литера А','')
      yunt = yunt.gsub(', литера А','')
      
      File.open("public/mirkvartir_s.xml", 'w') do |file|
        file.puts sevdol
      end 
      File.open("public/mirkvartir_y.xml", 'w') do |file|
        file.puts yunt
      end 
      write_log ['MIRKVARTIR ОК +++++++++++++++++']  
    rescue => error
      write_log ['MIRKVARTIR ERROR ================', error]      
    end   
    
    
    def perform_gdeetotdom
      all = get_file "all_ok.xml"
      all = all.gsub("245-37-59","245-34-15")
      all = all.gsub("385-27-78","385-06-65")
      File.open("public/all_ok_gdeetotdom.xml", 'w') do |file|
        file.puts all
      end 
      write_log ['GDEETOTDOM ОК +++++++++++++++++']  
    rescue => error
      write_log ['GDEETOTDOM ERROR ================', error]      
    end   
    
    
    def get_file file
      IO.read("public/#{file}")
    end
    
    private
    
    def write_log msg
      File.open("public/log.txt", 'a') do |file|
        file.write "#{Time.now + 3.hours} #{@ip}  "       
        msg.each do |m|
          file.puts m       
        end  
      end     
    end
   
    
    #PANORAMA%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
#    def flatten_panorama buildings
#      reparsed = []
#      @tmp = []
#      @info = {}
#      buildings.each do |build|
#        build[1].each do |objects|
#          objects['Objects'].each do |objects1|
#            objects1[1].each do |object|
#              object['Flats'].each do |flats|
#                flats[1].each do |flat|
#                  info = {
#                    build: objects['Build'],
#                    plandate: objects['Plandate'],
#                    dogovor: objects['Dogovor'],
#                    otdelka: objects['Otdelka'],
#                    payment: objects['Payment'],
#                    bank: objects['Bank'],
#                    discount: objects['Discount'],
#                    section: object['Section'],
#                    floor: object['Floor'],
#                  }
##                  unless flat.class == Hash
##                    @tmp << flat
##                    @info = @info.merge info
##                  else
##                    last = flat.merge(info)
##                    reparsed << last
##                  end  
#                  if flat.class == Hash && flat['State'] == 'Свободна' && info[:floor].to_i > 2
#                    last = flat.merge(info)
#                    last[:address] =
#                      case last[:build]
#                      when '1 корпус'  
#                        'ул. Шкапина, дом 9-11, литера А'
#                      when '2 корпус'  
#                        'ул. Шкапина, дом 9-11, литера А'
#                      when '3 корпус'  
#                        'ул. Розенштейна, дом 18, литера А'
#                      when '4 корпус'  
#                        'ул. Шкапина, дом 13, литера А'
#                      end    
#                    last[:url] = "http://panorama360spb.ru/planirovki-i-ceny/#{last[:build].to_i}-korpus/#{last[:floor].to_i}-floor/#{last[:section].to_i}-section/#{last['Num'].to_i}/"
#                    last[:img] = get_image_url last[:url]
#                    last[:img] = "http://panorama360spb.ru" + last[:img] if last[:img]
#                    reparsed << last
#                  end    
#                end
#              end
#            end
#          end
#        end
#      end
##      reparsed << Hash[@tmp].merge(@info)
#      reparsed.sort_by { |r| r['Id'].to_s }
#    end

#    def write_xml_panorama offers
#      panorama = ""
#        panorama << '<?xml version="1.0" encoding="utf-8"?>'
#        panorama << '<realty-feed xmlns="http://webmaster.yandex.ru/schemas/feed/realty/2010-06">'
#        panorama << "<generation-date>#{Time.now.to_datetime}</generation-date>"
#        offers.each do |offer|
#          panorama << "<offer internal-id=\"p_#{offer['Id']}\">"
#          panorama << "<type>продажа</type>"
#          panorama << "<property-type>жилая</property-type>"
#          panorama << "<category>квартира</category>"
#          panorama << "<area>"
#          panorama << "<value>#{offer['Tsquare']}</value>"
#          panorama << "<unit>кв.м</unit>"
#          panorama << "</area>"
#          panorama << "<living-space>"
#          panorama << "<value>#{offer['Lsquare']}</value>"
#          panorama << "<unit>кв.м</unit>"
#          panorama << "</living-space>"         
#          panorama << "<url>#{offer[:url]}</url>"
#          panorama << "<creation-date>#{Time.now.to_datetime}</creation-date>"
#          panorama << "<yandex-building-id>36143</yandex-building-id>"
#          panorama << "<building-name>Панорама 360</building-name>"
#          panorama << "<building-state>hand-over</building-state>"
#          panorama << "<built-year>2014</built-year>"
#          panorama << "<ready-quarter>4</ready-quarter>"
#              
#          panorama << "<floors-total>11</floors-total>"
#          
#          panorama << "<location>"
#            panorama << "<country>Россия</country>"
#            panorama << "<locality-name>Санкт-Петербург</locality-name>"
#            panorama << "<sub-locality-name>Адмиралтейский район</sub-locality-name>"
#            panorama << "<address>#{offer[:address]}</address>"
#            panorama << "<metro>"
#              panorama << "<name>Балтийская</name>"
#              panorama << "<time-on-foot>5</time-on-foot>"
#              panorama << "<time-on-transport>1</time-on-transport>"
#            panorama << "</metro>"
#          panorama << "</location>"
#
#          
#          panorama << "<sales-agent>"
#            panorama << "<phone>8-812-680-4040</phone>"
#            panorama << "<name>ООО Главстрой-СПб</name>"
#            panorama << "<organization>ООО Главстрой-СПб</organization>"
#            panorama << "<category>застройщик</category>"
#            panorama << "<url>http://panorama360spb.ru</url>"
#            panorama << "<photo>http://www.imageup.ru/img225/2207802/logo.png</photo>"
#          panorama << "</sales-agent>"
#         
#          panorama << "<price>"
#            panorama << "<value>#{offer['Dvalue']}</value>"
#            panorama << "<currency>RUR</currency>"
#          panorama << "</price>"
#                  
#          panorama << "<new-flat>1</new-flat>"
#          panorama << "<rooms>#{offer['Type'].to_i}</rooms>"
#          panorama << "<balcony>1</balcony>" unless offer['Balconysquare'].to_i == 0       
##          panorama << "<bathroom-unit>1</bathroom-unit>" unless offer['Balconysquare'].to_i == 0       
#          panorama << "<floor>#{offer[:floor].to_i}</floor>"        
#          panorama << "<image>#{offer[:img]}</image>" if offer[:img]
#          panorama << "<image>http://www.imageup.ru/img293/2206013/panorama.jpg</image>"        
#          panorama << "<image>http://www.imageup.ru/img293/2206014/panorama_1.jpg</image>"        
#          panorama << "<image>http://www.imageup.ru/img293/2206015/panorama_2.jpg</image>"        
#          panorama << "<image>http://www.imageup.ru/img293/2206016/panorama_3.jpg</image>"        
#          
#          panorama << "</offer>"
#        end  
#        panorama << '</realty-feed>'
#      begin  
#        File.open("public/panorama_ok.xml", 'w') { |file|  file.puts panorama }
#      rescue
#        sleep 10
#        File.open("public/panorama_ok.xml", 'w') { |file|  file.puts panorama }
#      end 
#    end
    
    def get_image_url url
      uri = URI.parse(url)
      doc = Nokogiri::HTML(open(url)) 
      img = doc.css(".flatcard-gallery .flatcard-gallery-big-image img")[0]
      if img
        uri.scheme + '://' + uri.host + img['src'] 
      end
    rescue
      false
    end    
    
    #SEVDOL%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
    def flatten_sevdol queue
      reparsed = []
      queue.each do |buildings|
        buildings['Buildings'].each do |building|
          building[1].each do |build|
            build['Objects'].each do |objects|
              objects[1].each do |object|
                object["Flats"]['Flat'].each do |flat|
                  info = {
                    queue: buildings["Nqueue"],
                    build: build['Build'],
                    plandate: build['Plandate'],
                    dogovor: build['Dogovor'],
                    otdelka: build['Otdelka'],
                    payment: build['Payment'],
                    bank: build['Bank'],
                    discount: build['Discount'],
                    section: object['Section'],
                    floor: object['Floor'],
                    }
                  if flat.class == Hash
                    last = flat.merge(info)
                    last['Id'] = 's_' +  last['Id']
                    reparsed << last
                  end                       
                end
              end
            end if build.class == Hash
          end
        end
      end  
      #reparsed += flatten_sevdol_other queue
      r = reparsed.find_all { |r| r['State'] == "Свободна" && r['Dvalue'].to_i > 0 }.sort_by { |r| r['Id'].to_s }.compact  
      r.each do |item|
        item[:address] = sev_addr(item[:queue], item[:build])
#          if item[:build].to_i == 1 
#            'Санкт-Петербург, пос. Парголово, ул. Николая Рубцова, дом 13 литера А'
#          elsif item[:build].to_i == 7
#            'Санкт-Петербург, пос. Парголово, Заречная ул., дом 35 корпус 1 литера А'
#          elsif item[:build].to_i == 8
#            'Санкт-Петербург, пос. Парголово, Заречная ул., дом 37 литера А'
#          else
#            'Санкт-Петербург, пос. Парголово, Заречная ул., дом 37 литера А'
#          end  
          
        item[:b_state] = 
          if item[:plandate]['Введён']
            'hand-over'
          else
            'built'
          end     
        item[:url] = "https://sevdol.ru/planirovki-i-ceny/#{item[:queue].to_i}-ochered-#{item[:build].to_i}-korpus/#{item['Num'].to_i}/"
        if @imgs[item['Id']] && !@imgs[item['Id']].blank?
          item[:img] = @imgs[item['Id']]
          #ap @imgs[item['Id']]
        else
          item[:img] = get_image_url(item[:url])
          unless item[:img].to_s.strip.blank?
            item[:img] = item[:img] 
          else
            item[:img] = nil
          end  
        end  
      end
      r = r.find_all { |r| r[:img] && r[:img] != 'http://sevdol.ru' }.compact
      r
    end
     
    
#    def flatten_sevdol_other queue
#      reparsed = []
#      queue.each do |buildings|
#        buildings['Buildings'].each do |building|
#          build = building[1].find_all { |b| b.class != Hash }
#          if build.size > 0
#            build[0][1]['Object'].each do |object|
#              object["Flats"]['Flat'].each do |flat|
#                info = {
#                  queue: buildings["Nqueue"],
#                  build: build[1][1],
#                  plandate: build[3][1],
#                  dogovor: build[4][1],
#                  otdelka: build[5][1],
#                  payment: build[6][1],
#                  bank: build[7][1],
#                  discount: build[8][1],
#                  section: object['Section'],
#                  floor: object['Floor'],
#                  }
#                reparsed << flat.merge(info) if flat.class == Hash
#              end
#            end
#          end
#        end
#      end  
#      reparsed.find_all { |r| r['State'] != "Бронь" && r['State'] != "Продана"}.compact
#    end    
    
    def write_xml_sevdol offers, fn = nil
      sevdol = ""
        sevdol << '<?xml version="1.0" encoding="utf-8"?>'
        sevdol << '<realty-feed xmlns="http://webmaster.yandex.ru/schemas/feed/realty/2010-06">'
        sevdol << "<generation-date>#{Time.now.to_datetime}</generation-date>"
        offers.each do |offer|
          sevdol << "<offer internal-id=\"#{offer['Id']}\">\n"
          sevdol << "<type>продажа</type>\n"
          sevdol << "<property-type>жилая</property-type>\n"
          sevdol << "<category>квартира</category>\n"
          sevdol << "<area>"
          sevdol << "<value>#{offer['Tsquare']}</value>"
          sevdol << "<unit>кв.м</unit>"
          sevdol << "</area>\n"
          sevdol << "<living-space>\n"
          sevdol << "<value>#{offer['Lsquare']}</value>"
          sevdol << "<unit>кв.м</unit>"
          sevdol << "</living-space>\n"
          sevdol << "<kitchen-space> <value>#{offer['Kitchensquare']}</value> <unit>кв.м</unit> </kitchen-space>\n"
          sevdol << "<bathroom-unit>совмещенный</bathroom-unit>\n"
          
        
          sevdol << "<url>#{offer[:url]}</url>\n"
          sevdol << "<creation-date>#{Time.now.to_datetime}</creation-date>\n"
          sevdol << "<last-update-date>#{Time.now.to_datetime}</last-update-date>\n"
          sevdol << "<yandex-building-id>36779</yandex-building-id>\n"
          sevdol << "<building-name>Северная долина</building-name>\n"
          sevdol << "<building-section>#{offer[:build].to_i}</building-section>\n"
          sevdol << "<building-type>кирпично-монолитный</building-type>\n"
          sevdol << "<building-section-id>s_#{offer[:build].to_i}</building-section-id>\n"
          
          sevdol << "<building-state>#{offer[:b_state]}</building-state>"
          b = offer[:plandate].scan(/\d/).join
          sevdol << "<built-year>#{offer[:b_state] == 'hand-over' ? '2015' : (b == '214' ? '2019' : b)}</built-year>\n"
          sevdol << "<ready-quarter>3</ready-quarter>\n"
                 
          sevdol << "<ceiling-height>2.5</ceiling-height>\n"
          sevdol << "<floors-total>27</floors-total>\n"
          
          sevdol << "<location>\n"
            sevdol << "<country>Россия</country>\n"
            sevdol << "<region>Санкт-Петербург</region>\n"
            sevdol << "<locality-name>Санкт-Петербург</locality-name>\n"
            sevdol << "<sub-locality-name>Выборгский район</sub-locality-name>\n"
            sevdol << "<address>#{offer[:address]}</address>\n"
            sevdol << "<metro>\n"
              sevdol << "<name>Парнас</name>\n"
              sevdol << "<time-on-foot>20</time-on-foot>\n"
              sevdol << "<time-on-transport>9</time-on-transport>\n"
            sevdol << "</metro>\n"
          sevdol << "</location>\n"

          
          sevdol << "<sales-agent>"
            sevdol << "<phone>+7 (812) 245-37-59</phone>\n"
            sevdol << "<name>ООО Главстрой-СПб</name>\n"
            sevdol << "<organization>ООО Главстрой-СПб</organization>\n"
            sevdol << "<category>застройщик</category>\n"
            sevdol << "<url>http://sevdol.ru</url>\n"
            sevdol << "<photo>http://www.imageup.ru/img225/2207802/logo.png</photo>\n"
          sevdol << "</sales-agent>\n"
         
          sevdol << "<price>\n"
            sevdol << "<value>#{offer['Dvalue']}</value>"
            sevdol << "<currency>RUR</currency>"
          sevdol << "</price>\n"
                  
          sevdol << "<mortgage>1</mortgage>\n"
          sevdol << "<new-flat>1</new-flat>\n"
          sevdol << "<rooms>#{offer['Type'].to_i}</rooms>\n"
          sevdol << "<studio>1</studio>\n" if offer['Type'] == '1 st'
          sevdol << "<balcony>1</balcony>\n" unless offer['Balconysquare'].to_i == 0             
          sevdol << "<floor>#{offer[:floor].to_i}</floor>\n"        
          sevdol << "<renovation>полная</renovation>\n"
          sevdol << "<image>#{offer[:img]}</image>\n" if offer[:img]
          sevdol << "<image>http://www.imageup.ru/img225/2207804/sevdol1.jpg</image>\n"        
          sevdol << "<image>http://www.imageup.ru/img225/2207806/sevdol2.jpg</image>\n"        
          sevdol << "<image>http://www.imageup.ru/img225/2207807/sevdol3.jpg</image>\n"               
          
          sevdol << "</offer>\n"
        end        
        sevdol << '</realty-feed>'
      begin  
        File.open("public/#{fn || 'sevdol_ok.xml'}", 'w'){ |file|  file.puts sevdol }
      rescue
        sleep 10
        File.open("public/sevdol_ok.xml", 'w') { |file|  file.puts sevdol }
      end 
    end  
    
    
    #YUNTOLOVO%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
    def flatten_yunt queue
      reparsed = []
      queue.reject { |q| q["Nqueue"].to_i == 1 }.each do |buildings|
        buildings['Buildings']['Building'].each do |build|
          build['Objects'].each do |objects|
            objects[1].each do |object|
              object["Flats"]['Flat'].each do |flat|
                info = {
                  queue: buildings["Nqueue"],
                  build: build['Build'],
                  plandate: build['Plandate'],
                  dogovor: build['Dogovor'],
                  otdelka: build['Otdelka'],
                  payment: build['Payment'],
                  bank: build['Bank'],
                  discount: build['Discount'],
                  section: object['Section'],
                  floor: object['Floor'],
                  }
                if flat.class == Hash && flat['State'] != "Бронь" && flat['State'] != "Продана" 
                  last = flat.merge(info)
                  last['Id'] = 'y_' + last['Id']
                  last[:b_state] = 
                    if last[:plandate]['Введён']
                      'hand-over'
                    else
                      'built'
                    end
                  last[:floors] =
                    if last[:queue].to_i == 1 && [3, 4, 5, 6, 7, 8, 9, 10, 11, 19, 20, 24, 25, 26].include?(last[:build].to_i)
                      '5'
                    elsif last[:queue].to_i == 2 && [1, 2, 18, 13].include?(last[:build].to_i)
                      '6'
                    else
                      '11'
                    end                    

                  last[:address] = yunt_addr(last[:queue], last[:build])
#                    if last[:build].to_i == 3
#                      'Санкт-Петербург, Юнтоловский проспект, д.53 корпус 3'
#                    elsif last[:build].to_i == 4
#                      'Санкт-Петербург, Юнтоловский проспект, д.51 корпус 3'
#                    elsif last[:build].to_i == 5
#                      'Санкт-Петербург, Юнтоловский проспект, д.51 корпус 4'
#                    elsif last[:build].to_i == 7
#                      'Санкт-Петербург, Юнтоловский проспект, д.49 корпус 7'
#                    elsif last[:build].to_i == 9
#                      'Санкт-Петербург, Юнтоловский проспект, д.49 корпус 5'
#                    elsif last[:build].to_i == 19
#                      'Санкт-Петербург, Юнтоловский проспект, д.47 корпус 3'
#                    elsif last[:build].to_i == 20
#                      'Санкт-Петербург, Юнтоловский проспект, д.47 корпус 2'
#                    elsif last[:build].to_i == 24
#                      'Санкт-Петербург, Юнтоловский проспект, д.47 корпус 5'
#                    else
#                      'Санкт-Петербург, Юнтоловский проспект, д.47 корпус 5'
#                    end
                  last[:url] = "http://yuntolovo-spb.ru/planirovki-i-ceny/#{last[:queue].to_i}-ochered-#{last[:build].to_i}-korpus/#{last['Num'].to_i}/"
                  if @imgs[last['Id']]  && !@imgs[last['Id']].blank?
                    last[:img] = @imgs[last['Id']]
                  else
                    last[:img] = get_image_url last[:url]
                    unless last[:img].blank?
                      last[:img] = last[:img] 
                    else
                      last[:img] = nil
                    end  
                  end 
                  reparsed << last
                end                   
              end
            end
          end
        end
      end  
      r = reparsed.find_all { |r| r[:img] }.sort_by { |r| r['Id'].to_s }.compact
      r
    end

    def write_xml_yuntolovo offers, fn = nil
        yuntolovo = ""
        yuntolovo << '<?xml version="1.0" encoding="utf-8"?>'
        yuntolovo << '<realty-feed xmlns="http://webmaster.yandex.ru/schemas/feed/realty/2010-06">'
        yuntolovo << "<generation-date>#{Time.now.to_datetime}</generation-date>"
        offers.each do |offer|
          if offer[:img]
            yuntolovo << "<offer internal-id=\"#{offer['Id']}\">\n"
            yuntolovo << "<type>продажа</type>\n"
            yuntolovo << "<property-type>жилая</property-type>\n"
            yuntolovo << "<category>квартира</category>\n"
            yuntolovo << "<area>\n"
            yuntolovo << "<value>#{offer['Tsquare']}</value>\n"
            yuntolovo << "<unit>кв.м</unit>\n"
            yuntolovo << "</area>\n"
            yuntolovo << "<living-space>\n"
            yuntolovo << "<value>#{offer['Lsquare']}</value>\n"
            yuntolovo << "<unit>кв.м</unit>\n"
            yuntolovo << "</living-space>\n"
            yuntolovo << "<kitchen-space> <value>#{offer['Kitchensquare']}</value> <unit>кв.м</unit> </kitchen-space>\n"
            yuntolovo << "<bathroom-unit>совмещенный</bathroom-unit>\n"
            
            yuntolovo << "<url>#{offer[:url]}</url>\n"
            yuntolovo << "<creation-date>#{Time.now.to_datetime}</creation-date>\n"
            yuntolovo << "<last-update-date>#{Time.now.to_datetime}</last-update-date>\n"
            yuntolovo << "<yandex-building-id>37721</yandex-building-id>\n"
            yuntolovo << "<building-name>Юнтолово</building-name>\n"
            yuntolovo << "<building-section>#{offer[:build].to_i}</building-section>\n"
            yuntolovo << "<building-type>кирпично-монолитный</building-type>\n"
            yuntolovo << "<building-section-id>y_#{offer[:build].to_i}</building-section-id>\n"
            
          
            yuntolovo << "<building-state>#{offer[:b_state]}</building-state>\n"
            yuntolovo << "<built-year>#{offer[:b_state] == 'hand-over' ? '2014' : offer[:plandate].scan(/\d/).join}</built-year>\n"
            yuntolovo << "<ready-quarter>4</ready-quarter>\n"

            yuntolovo << "<ceiling-height>2.5</ceiling-height>\n"
            yuntolovo << "<floors-total>#{offer[:floors]}</floors-total>\n"

            yuntolovo << "<location>\n"
              yuntolovo << "<country>Россия</country>\n"
              yuntolovo << "<locality-name>Санкт-Петербург</locality-name>\n"
              yuntolovo << "<region>Санкт-Петербург</region>\n"
              yuntolovo << "<sub-locality-name>Приморский район</sub-locality-name>\n"
              yuntolovo << "<address>#{offer[:address]}</address>\n"
              yuntolovo << "<metro>\n"
                yuntolovo << "<name>Старая деревня</name>\n"
                yuntolovo << "<time-on-transport>24</time-on-transport>\n"
              yuntolovo << "</metro>\n"
            yuntolovo << "</location>\n"

            yuntolovo << "<sales-agent>"
              yuntolovo << "<phone>+7 (812) 385-27-78</phone>\n"
              yuntolovo << "<name>ООО Главстрой-СПб</name>\n"
              yuntolovo << "<organization>ООО Главстрой-СПб</organization>\n"
              yuntolovo << "<category>застройщик</category>\n"
              yuntolovo << "<url>http://yuntolovo-spb.ru</url>\n"
              yuntolovo << "<photo>http://www.imageup.ru/img225/2207802/logo.png</photo>\n"
            yuntolovo << "</sales-agent>\n"

            yuntolovo << "<price>\n"
              yuntolovo << "<value>#{offer['Dvalue'].to_i}</value>\n"
              yuntolovo << "<currency>RUR</currency>\n"
            yuntolovo << "</price>\n"

            yuntolovo << "<mortgage>1</mortgage>\n"
            yuntolovo << "<new-flat>1</new-flat>\n"
            yuntolovo << "<rooms>#{offer['Type'].to_i}</rooms>\n"
            yuntolovo << "<studio>1</studio>\n" if offer['Type'] == '1 st'
            yuntolovo << "<balcony>1</balcony>\n" unless offer['Balconysquare'].to_i == 0             
            yuntolovo << "<floor>#{offer[:floor].to_i}</floor>\n"        
            yuntolovo << "<renovation>полная</renovation>\n"
            yuntolovo << "<image>#{offer[:img]}</image>\n"
            yuntolovo << "<image>http://www.imageup.ru/img293/2206027/yunt1.jpg</image>\n"        
            yuntolovo << "<image>http://www.imageup.ru/img293/2206038/yunt2.jpg</image>\n"        
            yuntolovo << "<image>http://www.imageup.ru/img293/2206041/yunt3.jpg</image>\n"               

            yuntolovo << "</offer>\n"
          end  
        end          
        yuntolovo << '</realty-feed>'
    begin    
      File.open("public/#{fn || 'yuntolovo_ok.xml'}", 'w') { |file| file.puts yuntolovo }
    rescue
      sleep 10
      File.open("public/yuntolovo_ok.xml", 'w') { |file| file.puts yuntolovo }
    end    
    end

    #novostrojki %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    def write_xml_novostrojki offers
        novostrojki = ""
        novostrojki << '<?xml version="1.0" encoding="utf-8"?>'
        novostrojki << '<realty-feed xmlns="http://webmaster.yandex.ru/schemas/feed/realty/2010-06">'
        novostrojki << "<generation-date>#{Time.now.to_datetime}</generation-date>"
        offers.each do |offer|
          if offer[:img]
            novostrojki << "<offer internal-id=\"#{offer['Id']}\">"
            novostrojki << "<type>продажа</type>"
            novostrojki << "<property-type>жилая</property-type>"
            novostrojki << "<category>квартира</category>"
            novostrojki << "<area>"
            novostrojki << "<value>#{offer['Tsquare']}</value>"
            novostrojki << "<unit>кв.м</unit>"
            novostrojki << "</area>"
            novostrojki << "<living-space>"
            novostrojki << "<value>#{offer['Lsquare']}</value>"
            novostrojki << "<unit>кв.м</unit>"
            novostrojki << "</living-space>"
            novostrojki << "<kitchen-space> <value>#{offer['Kitchensquare']}</value> <unit>кв.м</unit> </kitchen-space>"
            novostrojki << "<bathroom-unit>совмещенный</bathroom-unit>"
            
            novostrojki << "<url>#{offer[:url]}</url>"
            novostrojki << "<creation-date>#{Time.now.to_datetime}</creation-date>"
            novostrojki << "<last-update-date>#{Time.now.to_datetime}</last-update-date>"
            novostrojki << "<yandex-building-id>#{offer['Id'][0] == "s" ? '36779' : '37721'}</yandex-building-id>"
            novostrojki << "<building-name>#{offer['Id'][0] == "s" ? 'Северная долина' : 'Юнтолово'}</building-name>"
            novostrojki << "<building-section>#{offer[:build].to_i}</building-section>"
            novostrojki << "<building-type>кирпично-монолитный</building-type>"
            novostrojki << "<building-section-id>y_#{offer[:build].to_i}</building-section-id>"
            
          
            novostrojki << "<building-state>#{offer[:b_state]}</building-state>"
            novostrojki << "<built-year>#{offer[:b_state] == 'hand-over' ? '2014' : '2016'}</built-year>"
            novostrojki << "<ready-quarter>#{offer[:b_state] == 'hand-over' ? '4' : '3'}</ready-quarter>"

            novostrojki << "<ceiling-height>2.5</ceiling-height>"
            novostrojki << "<floors-total>#{offer[:floors]}</floors-total>"

            novostrojki << "<location>"
              novostrojki << "<country>Россия</country>"
              novostrojki << "<locality-name>Санкт-Петербург</locality-name>"
              novostrojki << "<region>Санкт-Петербург</region>"
              novostrojki << "<sub-locality-name>#{offer['Id'][0] == "s" ? 'Выборгский район' : 'Приморский район'}</sub-locality-name>"
              novostrojki << "<address>#{offer[:address]}</address>"
              novostrojki << "<metro>"
                novostrojki << "<name>#{offer['Id'][0] == "s" ? 'Парнас' : 'Старая деревня'}</name>"
                novostrojki << "<time-on-transport>#{offer['Id'][0] == "s" ? '9' : '24'}</time-on-transport>"
              novostrojki << "</metro>"
            novostrojki << "</location>"

            novostrojki << "<sales-agent>"
              novostrojki << "<phone>+7 (812) #{offer['Id'][0] == "s" ? '245-37-59' : '385-27-78'}</phone>"
              novostrojki << "<name>ООО Главстрой-СПб</name>"
              novostrojki << "<organization>ООО Главстрой-СПб</organization>"
              novostrojki << "<category>застройщик</category>"
              novostrojki << "<url>#{offer['Id'][0] == "s" ? 'http://sevdol.ru' : 'http://yuntolovo-spb.ru'}</url>"
              novostrojki << "<photo>http://www.imageup.ru/img225/2207802/logo.png</photo>"
            novostrojki << "</sales-agent>"

            novostrojki << "<price>"
              novostrojki << "<value>#{offer['Dvalue'].to_i}</value>"
              novostrojki << "<currency>RUR</currency>"
            novostrojki << "</price>"

            novostrojki << "<mortgage>1</mortgage>"
            novostrojki << "<new-flat>1</new-flat>"
            novostrojki << "<rooms>#{offer['Type'].to_i == 1 && (offer['Tsquare'].to_f - offer['Lsquare'].to_f < 20) ? 0 : offer['Type'].to_i}</rooms>"
            novostrojki << "<balcony>1</balcony>" unless offer['Balconysquare'].to_i == 0             
            novostrojki << "<floor>#{offer[:floor].to_i}</floor>"        
            novostrojki << "<renovation>полная</renovation>"
            novostrojki << "<image>#{offer[:img]}</image>"
            novostrojki << "<image>http://www.imageup.ru/img293/2206027/yunt1.jpg</image>"        
            novostrojki << "<image>http://www.imageup.ru/img293/2206038/yunt2.jpg</image>"        
            novostrojki << "<image>http://www.imageup.ru/img293/2206041/yunt3.jpg</image>"               

            novostrojki << "</offer>"
          end  
        end          
        novostrojki << '</realty-feed>'
    begin    
      File.open("public/novostrojki.xml", 'w') { |file| file.puts novostrojki }
    rescue
      sleep 10
      File.open("public/novostrojki.xml", 'w') { |file| file.puts novostrojki }
    end    
    end

    #DOMOFOND %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    def write_xml_domofond offers
        domofond = ""
        domofond << '<?xml version="1.0" encoding="utf-8"?>'
        domofond << '<realty-feed xmlns="http://webmaster.yandex.ru/schemas/feed/realty/2010-06">'
        domofond << "<generation-date>#{Time.now.to_datetime}</generation-date>"
        offers.each do |offer|
          if offer[:img]
            domofond << "<offer internal-id=\"#{offer['Id']}\">"
            domofond << "<type>продажа</type>"
            domofond << "<property-type>жилая</property-type>"
            domofond << "<category>квартира</category>"
            domofond << "<area>"
            domofond << "<value>#{offer['Tsquare']}</value>"
            domofond << "<unit>кв.м</unit>"
            domofond << "</area>"
            domofond << "<living-space>"
            domofond << "<value>#{offer['Lsquare']}</value>"
            domofond << "<unit>кв.м</unit>"
            domofond << "</living-space>"
            domofond << "<kitchen-space> <value>#{offer['Kitchensquare']}</value> <unit>кв.м</unit> </kitchen-space>"
            domofond << "<bathroom-unit>совмещенный</bathroom-unit>"
            
            domofond << "<url>#{offer[:url]}</url>"
            domofond << "<creation-date>#{Time.now.to_datetime}</creation-date>"
            domofond << "<last-update-date>#{Time.now.to_datetime}</last-update-date>"
            domofond << "<yandex-building-id>#{offer['Id'][0] == "s" ? '36779' : '37721'}</yandex-building-id>"
            domofond << "<building-name>#{offer['Id'][0] == "s" ? 'Северная долина' : 'Юнтолово'}</building-name>"
            domofond << "<building-section>#{offer[:build].to_i}</building-section>"
            domofond << "<building-type>кирпично-монолитный</building-type>"
            domofond << "<building-section-id>y_#{offer[:build].to_i}</building-section-id>"
            
          
            domofond << "<building-state>#{offer[:b_state]}</building-state>"
            domofond << "<built-year>#{offer[:b_state] == 'hand-over' ? '2014' : '2016'}</built-year>"
            domofond << "<ready-quarter>#{offer[:b_state] == 'hand-over' ? '4' : '3'}</ready-quarter>"

            domofond << "<ceiling-height>2.5</ceiling-height>"
            domofond << "<floors-total>#{offer[:floors]}</floors-total>"

            domofond << "<location>"
              domofond << "<country>Россия</country>"
              domofond << "<locality-name>Санкт-Петербург</locality-name>"
              domofond << "<region>Санкт-Петербург</region>"
              domofond << "<sub-locality-name>#{offer['Id'][0] == "s" ? 'Выборгский район' : 'Приморский район'}</sub-locality-name>"
              domofond << "<address>#{offer[:address]}</address>"
              domofond << "<metro>"
                domofond << "<name>#{offer['Id'][0] == "s" ? 'Парнас' : 'Старая деревня'}</name>"
                domofond << "<time-on-transport>#{offer['Id'][0] == "s" ? '9' : '24'}</time-on-transport>"
              domofond << "</metro>"
            domofond << "</location>"

            domofond << "<sales-agent>"
              domofond << "<phone>+7 (812) #{offer['Id'][0] == "s" ? '245-34-89' : '385-27-44'}</phone>"
              domofond << "<name>ООО Главстрой-СПб</name>"
              domofond << "<organization>ООО Главстрой-СПб</organization>"
              domofond << "<category>застройщик</category>"
              domofond << "<url>#{offer['Id'][0] == "s" ? 'http://sevdol.ru' : 'http://yuntolovo-spb.ru'}</url>"
              domofond << "<photo>http://www.imageup.ru/img225/2207802/logo.png</photo>"
            domofond << "</sales-agent>"

            domofond << "<price>"
              domofond << "<value>#{offer['Dvalue'].to_i}</value>"
              domofond << "<currency>RUR</currency>"
            domofond << "</price>"

            domofond << "<mortgage>1</mortgage>"
            domofond << "<new-flat>1</new-flat>"
            domofond << "<rooms>#{offer['Type'].to_i == 1 && (offer['Tsquare'].to_f - offer['Lsquare'].to_f < 20) ? 0 : offer['Type'].to_i}</rooms>"
            domofond << "<balcony>1</balcony>" unless offer['Balconysquare'].to_i == 0             
            domofond << "<floor>#{offer[:floor].to_i}</floor>"        
            domofond << "<renovation>полная</renovation>"
            domofond << "<image>#{offer[:img]}</image>"
            domofond << "<image>http://www.imageup.ru/img293/2206027/yunt1.jpg</image>"        
            domofond << "<image>http://www.imageup.ru/img293/2206038/yunt2.jpg</image>"        
            domofond << "<image>http://www.imageup.ru/img293/2206041/yunt3.jpg</image>"               

            domofond << "</offer>"
          end  
        end          
        domofond << '</realty-feed>'
    begin    
      File.open("public/all_ok_domo.xml", 'w') { |file| file.puts domofond }
    rescue
      sleep 10
      File.open("public/all_ok_domo.xml", 'w') { |file| file.puts domofond }
    end    
    end

    #MAILRU %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    def write_xml_mailru offers
      mailru = ""
      mailru << '<?xml version="1.0" encoding="UTF-8"?>'
      mailru << '<root>'
      mailru << '<offers>'

      offers.each do |offer|
        if offer[:img]
          mailru << "<offer>"
          mailru << "<id>#{offer['Id'].gsub("s","1").to_i}</id>"
          mailru << "<part>жилая</part>"
          mailru << "<object-type>квартира в новостройке</object-type>"
          mailru << "<description><![CDATA[
           Жилой комплекс «Северная долина» от «Главстрой-СПб» – это новый перспективный район Санкт-Петербурга. Здесь формируется среда, комфортная для жизни современного горожанина.
          Лучший комплекс 2014 г.
          Площадь территории - 270 га
          Жилая застройка – 2,7 млн кв. м
          Объекты обслуживания населения - 375 тыс. кв. м
          Бюджет проекта – 75 млрд руб.
          Количество жителей – 80 тысяч человек
          Помимо жилых домов застройщик «Главстрой-СПб» возводит детские садики, школы, спортивные объекты, организует игровые площадки и зоны отдыха, строит дороги, подземные и наземные паркинги.
          ]]></description>"
          mailru << "<url><![CDATA[#{offer[:url]}]]></url>"
          mailru << "<created-at>#{Time.now.to_datetime}</created-at>"
          mailru << "<updated-at>#{Time.now.to_datetime}</updated-at>"
          mailru << "<images>"
          mailru << "<image primary=\"1\"><![CDATA[#{offer[:img]}]]></image>"
          mailru << "<image primary=\"0\"><![CDATA[http://www.imageup.ru/img225/2207804/sevdol1.jpg]]></image>"
          mailru << "<image primary=\"0\"><![CDATA[http://www.imageup.ru/img225/2207806/sevdol2.jpg]]></image>"
          mailru << "<image primary=\"0\"><![CDATA[http://www.imageup.ru/img225/2207807/sevdol3.jpg]]></image>"  
          mailru << "</images>"
          mailru << "<bargain>"
          mailru << "<type>продажа</type>"
          mailru << "<mortgage>да</mortgage>"
          mailru << "<price>"
          mailru << "<value>#{offer['Dvalue'].to_i}</value>"
          mailru << "<unit>total</unit>"
          mailru << "<currency>RUR</currency>"
          mailru << "<period>total</period>"         
          mailru << "</price>"
          mailru << "</bargain>"
          mailru << "<seller>"
          mailru << "<phone>8 (812) 748-36-74</phone>"
          mailru << "<name><![CDATA[ООО Главстрой-СПб]]></name>"
          mailru << "<email><![CDATA[spbinfo@glavstroy.ru]]></email>"
          mailru << "</seller>"

          mailru << "<location>"
          mailru << "<country>Россия</country>"                
          mailru << "<state>Санкт-Петербург</state>"                
          mailru << "<town>Санкт-Петербург</town>"                
          mailru << "<town-region>Выборгский район</town-region>"                
          mailru << "<street>#{offer[:address].split(',')[2] if offer[:address]}</street>"                
          mailru << "<house>"                
          mailru << "<number>#{sev_addr_d(offer[:queue], offer[:build])}</number>"                
          mailru << "<build>#{sev_addr_lit(offer[:queue], offer[:build])}</build>"   
          mailru << "<struct>#{sev_addr_kor(offer[:queue], offer[:build])}</struct>"  
          mailru << "</house>"  
          mailru << "<subway>"  
          mailru << "<name>Парнас</name>"  
          mailru << "<distance>20</distance>"  
          mailru << "<distance-type>минут пешком</distance-type>"  
          mailru << "</subway>"  
          mailru << "<latitude>60.0693</latitude>"  
          mailru << "<longitude>30.3376</longitude>"  
          mailru << "</location>"

          mailru << "<floor>"
          mailru << "<total>27</total>"
          mailru << "<current>#{offer[:floor].to_i}</current>"
          mailru << "</floor>"
          mailru << "<area>"
          mailru << "<total>#{offer['Tsquare']}</total>"
          mailru << "<living>#{offer['Lsquare']}</living>"
          mailru << "<kitchen>#{offer['Kitchensquare']}</kitchen>"
          mailru << "</area>"
          mailru << "<house-info>"
          mailru << "<house-type>Кирпично-монолитный</house-type>"
          mailru << "<elevator>1</elevator>"
          mailru << "<newbuilding>"
          mailru << "<name>ЖК «Северная долина»</name>"
          mailru << "</newbuilding>"
          mailru << "</house-info>"
          mailru << "<object-info>"
          mailru << "<rooms>"
          mailru << "<total>#{offer['Type'].to_i}</total>"
          mailru << "</rooms>"
          mailru << "<ceil-height>2.5</ceil-height>"
          mailru << "</object-info>"         
          mailru << "</offer>"
        end  
      end          
      mailru << '</offers>'
      mailru << '</root>'
      File.open("public/mailru_flats.xml", 'w') { |file| file.puts mailru }
    end
        

    #MAILRU %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    def write_xml_mailru_nb offers
      mailru = ""
      mailru << '<?xml version="1.0" encoding="UTF-8"?>'
      mailru << '<root>'
      mailru << '<offers>'
      mailru << '<offer>'
      
      mailru << '<id>1</id>'
      mailru << '<part>жилая</part>'
      mailru << '<object-type>Новостройка</object-type>'
      mailru << "<description><![CDATA[
Жилой комплекс «Северная долина» от «Главстрой-СПб» – это новый перспективный район Санкт-Петербурга. Площадь территории - 270 га Жилая застройка – 2,7 млн кв. м Объекты обслуживания населения - 375 тыс. кв. м Бюджет проекта – 75 млрд руб. Количество жителей – 80 тысяч человек. 
Северо-западная часть «Северной Долины» граничит с Шуваловским парком.. 
Сегодня в «Северной долине» работают два государственных детских сада и два корпуса государственной общеобразовательной школы. В текущем году завершено строительство 2 дошкольных учреждений на 190 воспитанников каждый.
]]></description>"
      mailru << "<url><![CDATA[url	url	http://glavstroi-spb.ru]]></url>"
      mailru << "<created-at>#{Time.now.to_datetime}</created-at>"
      mailru << "<updated-at>#{Time.now.to_datetime}</updated-at>"
      mailru << '<images>'
      mailru << '<image primary="1"><![CDATA[http://www.imageup.ru/img225/2207804/sevdol1.jpg]]></image>'
      mailru << '<image primary="0"><![CDATA[http://www.imageup.ru/img225/2207806/sevdol2.jpg]]></image>'
      mailru << '<image primary="0"><![CDATA[http://www.imageup.ru/img225/2207807/sevdol3.jpg]]></image>'
      mailru << '</images>'
      mailru << '<bargain>'
      mailru << '<type>продажа</type>'
      mailru << '<mortgage>1</mortgage>'
      mailru << '<maternity-capital>1</maternity-capital>'
      mailru << '<military-mortgage>1</military-mortgage>'
      mailru << '<sales-started>1</sales-started>'
      mailru << '<payment-installment>1</payment-installment>'
      mailru << '<fz214>1</fz214>'
      mailru << '</bargain>'
      
      mailru << '<location>'
      mailru << '<country>Россия</country>'
      mailru << '<state>Санкт-Петербург</state>'
      mailru << "<town>Санкт-Петербург</town>"                
      mailru << "<town-region>Выборгский район</town-region>"    
      mailru << "<subway>"  
      mailru << "<name>Парнас</name>"  
      mailru << "<distance>20</distance>"  
      mailru << "<distance-type>минут пешком</distance-type>"  
      mailru << "</subway>"  
      mailru << "<latitude>60.0693</latitude>"  
      mailru << "<longitude>30.3376</longitude>"  
      mailru << '</location>'
      
      mailru << "<seller>"
      mailru << "<phone>8 (812) 604-04-04</phone>"
      mailru << "<name><![CDATA[ООО Главстрой-СПб]]></name>"
      mailru << "<email><![CDATA[spbinfo@glavstroy.ru]]></email>"
      mailru << "</seller>"      
      
      mailru << "<developer>"      
      mailru << "<name>ООО «Главстрой-СПб»</name>"      
      mailru << "<address>199034, Россия, Санкт-Петербург, Финляндский пр., д. 4, бизнес-центр «Петровский форт»</address>"      
      mailru << "<email>spbinfo@glavstroy.ru</email>"      
      mailru << "<url>http://glavstroi-spb.ru/</url>"      
      mailru << "<phone>+7 (812) 7483674</phone>"      
      mailru << "<logo-url>http://glavstroi-spb.ru/bitrix/templates/glavstroi-spb/img/company-logo.png</logo-url>"      
      mailru << "<description>ООО «Главстрой-СПб» – один из крупнейших российских девелоперов, реализующих масштабные проекты комплексного развития городских территорий. Признана одной из наиболее эффективных компаний Северо-Запада. Входит в состав одной из крупнейших в России промышленных групп «Базовый Элемент». </description>"      
      mailru << "</developer>"      
      
      mailru << "<house-info>"      
      mailru << "<house-type>Кирпично-монолитный</house-type>"      
      mailru << "<elevator>1</elevator>"      
      mailru << "<refuse>1</refuse>"      
      mailru << "<garage>1</garage>"      
      mailru << "<parking>1</parking>"      
      mailru << "<heating-type>Есть</heating-type>"      
      
      mailru << "<newbuilding>"      
      mailru << "<name>ЖК «Северная долина»</name>"      
      mailru << "<date>1 квартал 2017</date>"      
      mailru << "<status>Дом строится</status>"      
      mailru << "<new-building-class>эконом</new-building-class>"      
      
      mailru << "<housing-stock>"
      
      o1k = offers.find_all { |o| o['Type'].to_i == 1 }
      mailru << "<housing>"
      mailru << "<type>1</type>"
      mailru << "<area>#{ o1k.map { |o| o['Tsquare'] }.min}</area>"
      mailru << "<area-max>#{ o1k.map { |o| o['Tsquare'] }.max}</area-max>"
      mailru << "<price>#{ o1k.map { |o| o['Dvalue'].to_i }.min}</price>"
      mailru << "<price-max>#{ o1k.map { |o| o['Dvalue'].to_i }.max}</price-max>"
      mailru << "<count>#{o1k.length}</count>"
      mailru << "</housing>"
      
      o2k = offers.find_all { |o| o['Type'].to_i == 2 }
      mailru << "<housing>"
      mailru << "<type>2</type>"
      mailru << "<area>#{ o2k.map { |o| o['Tsquare'] }.min}</area>"
      mailru << "<area-max>#{ o2k.map { |o| o['Tsquare'] }.max}</area-max>"
      mailru << "<price>#{ o2k.map { |o| o['Dvalue'].to_i }.min}</price>"
      mailru << "<price-max>#{ o2k.map { |o| o['Dvalue'].to_i }.max}</price-max>"
      mailru << "<count>#{o2k.length}</count>"
      mailru << "</housing>"
      
      o3k = offers.find_all { |o| o['Type'].to_i == 3 }
      mailru << "<housing>"
      mailru << "<type>3</type>"
      mailru << "<area>#{ o3k.map { |o| o['Tsquare'] }.min}</area>"
      mailru << "<area-max>#{ o3k.map { |o| o['Tsquare'] }.max}</area-max>"
      mailru << "<price>#{ o3k.map { |o| o['Dvalue'].to_i }.min}</price>"
      mailru << "<price-max>#{ o3k.map { |o| o['Dvalue'].to_i }.max}</price-max>"
      mailru << "<count>#{o3k.length}</count>"
      mailru << "</housing>"
      
      mailru << "</housing-stock>"      
      
      mailru << "</newbuilding>"      
      
      mailru << "</house-info>"      

      mailru << "</offer>"
      mailru << '</offers>'
      mailru << '</root>'
        
      File.open("public/mailru_nb.xml", 'w') { |file| file.puts mailru }
    end
        
    
    #EMLS (CIAN) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    def write_xml_emls offers     

      emls = '<?xml version="1.0" encoding="windows-1251"?>'
      emls << '<!-- Generated by: http://www.emls.ru/ -->'
      emls << '<root>'
      emls << "<creation-date>#{Time.now.to_datetime}</creation-date>"
      emls << "<objects>"
      offers.each do |offer|
        emls << "<object>"
        emls << "<external_id>#{(offer['Id'][0] == 's' ? '9' : '0') + offer['Id'].scan(/\d/).join}</external_id>"
        emls << "<building>#{offer['Id'][0] == 's' ? 'Северная долина' : 'Юнтолово'}, #{ offer[:queue] }, корпус  #{offer[:build].to_i}</building>" 
        emls << "<city>С-Петербург</city>"
        emls << "<adres>#{offer[:address]}</adres>"
        emls << "<idn>#{offer['Num'].to_i}</idn>"
        emls << "<level>#{offer[:floor].to_i}</level>"
        emls << "<section>#{offer[:section].to_i}</section>"
        emls << "<kkv>#{offer['Type'].to_i}</kkv>"
        emls << "<so>#{offer['Tsquare']}</so>"
        emls << "<sl>#{offer['Lsquare']}</sl>"
        #emls << "<sk>12.1</sk>"
        #emls << "<sh>7.8+4.1+1.2</sh>"
        #emls << "<ss>1.1+3.22</ss>"
        #emls << "<sbal>2.1</sbal>"
        #emls << "<slodg></slodg>"
        emls << "<price>#{offer['Dvalue'].to_i}</price>"
        emls << "<status>в продаже</status>"
        emls << "<image_plan>#{offer[:img]}</image_plan>"           
        #emls << "<image_schema>http://www.emls.ru/spb/term/images/build/10259/070004961.jpg</image_schema>"           
        emls << "</object>"
      end        
      emls << "</objects>"
      emls << '</root>'
      begin  
        File.open("public/emls.xml", 'w:windows-1251'){ |file|  file.puts emls.force_encoding("utf-8").encode("windows-1251", undef: :replace) }
      rescue
        sleep 10
        File.open("public/emls.xml", 'w:windows-1251') { |file|  file.puts emls.force_encoding("utf-8").encode("windows-1251", undef: :replace) }
      end 
    end
    
    
    #SPBGURU %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    def write_xml_spbguru buildings     
      spbguru = '<?xml version="1.0" encoding="utf-8"?>'
      spbguru << '<feed>'
      spbguru << '<builds>'

      buildings.each do |build|
        spbguru << "<build>"
        spbguru << "<name>#{build[0]['Id'][0] == "s" ? 'Северная долина' : 'Юнтолово'}</name>"
        spbguru << "<apartments>"
        build.each do |offer|
          spbguru << "<apartment>"
          spbguru << "<area>#{offer['Tsquare']}</area>"
          spbguru << "<living_area>#{offer['Lsquare']}</living_area>"
          spbguru << "<rooms_count>#{offer['Type'].to_i == 1 && (offer['Tsquare'].to_f - offer['Lsquare'].to_f < 20) ? 'S' : offer['Type'].to_i}</rooms_count>"
          spbguru << "<toilet>Y</toilet>"
          spbguru << "<balcony>#{offer['Balconysquare'].to_i == 0 ? 'N' : 'Y'}</balcony>"
          spbguru << "<balcony_area>#{offer['Balconysquare'].to_i}</balcony_area>"
          spbguru << "<price>#{offer['Dvalue'].to_i}</price>"
          spbguru << "<image>#{offer[:img]}</image>"           
          spbguru << "<corpus>#{offer[:build].to_i}</corpus>"           
          spbguru << "<floor>#{offer[:floor].to_i}</floor>"
          spbguru << "<finish>Y</finish>"     
          spbguru << "</apartment>"
        end     
        spbguru << "</apartments>"
        spbguru << "</build>"
      end  
      spbguru << "</builds>"
      spbguru << '</feed>'
      begin  
        File.open("public/spbguru.xml", 'w'){ |file|  file.puts spbguru }
      rescue
        sleep 10
        File.open("public/spbguru.xml", 'w') { |file|  file.puts spbguru }
      end 
    end
    
    #BN%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    def write_xml_bn offers
      s_st = s_1 = s_2 = s_3 = y_st = y_1 = y_2 = y_3 = 0
      pts = '<print_to_string>1</print_to_string>'
      bn = '<?xml version="1.0" encoding="windows-1251"?>'
      bn << '<root>'
      bn << "<objects>"
      offers.each do |offer|
        bn << "<object>"
        bn << "<external_id>#{offer['Id'].gsub("s","1").gsub("y","2").scan(/\d/).join}</external_id>"
        if studia?(offer) && sevd?(offer) && s_st < 12
          bn << pts; s_st+=1
        elsif offer['Type'].to_i == 1 && sevd?(offer) && s_1 < 14
          bn << pts; s_1+=1
        elsif offer['Type'].to_i == 2 && sevd?(offer) && s_2 < 12
          bn << pts; s_2+=1
        elsif offer['Type'].to_i == 3 && sevd?(offer) && s_3 < 12
          bn << pts; s_3+=1
        elsif studia?(offer) && yunt?(offer) && y_st < 12
          bn << pts; y_st+=1
        elsif offer['Type'].to_i == 1 && yunt?(offer) && y_1 < 14
          bn << pts; y_1+=1
        elsif offer['Type'].to_i == 2 && yunt?(offer) && y_2 < 12
          bn << pts; y_2+=1
        elsif offer['Type'].to_i == 3 && yunt?(offer) && y_3 < 12
          bn << pts; y_3+=1
        end  
        deadline = offer[:plandate].gsub('квартал', 'кв.').gsub('*', '').gsub('20', '').strip
        deadline = 'сдан' if offer[:plandate]['Введён в эксплуатацию']
            
        bn << "<action_id>1</action_id>"
        bn << "<deadline>#{deadline}</deadline>"
        bn << "<type_id>14</type_id>"
        bn << "<country>RUS</country>"
        bn << "<area_id>35</area_id>"
        bn << "<region_id>#{offer['Id'][0] == "s" ? '3' : '11'}</region_id>"
        bn << "<metro_id>#{offer['Id'][0] == "s" ? '83' : '96'}</metro_id>"
        bn << "<housing_estate>#{offer['Id'][0] == "s" ? 'Северная долина' : 'Юнтолово'}</housing_estate>"
        bn << "#{offer['Id'][0] == "s" ? '<undist>15</undist><undist_id>1</undist_id>' : '<undist>50</undist><undist_id>2</undist_id>'}"     
        bn << "<short_description>#{offer['Id'][0] == "s" ? 'Рядом школы и детские сады!' : 'Малоэтажный комплекс рядом с Юнтоловским заповедником'}</short_description>"     
        bn << "<price>#{(offer['Dvalue'].to_i/1000).round}</price>"
        bn << "<price_type_id>22</price_type_id>"
        bn << "<address>#{offer[:address]}</address>"
        bn << "<level>#{offer[:floor].to_i}/#{offer['Id'][0] == "s" ? '27' : '14'}</level>" 
        bn << "<phone_kod>812</phone_kod>"
        bn << "<phone>#{offer['Id'][0] == "s" ? ' 6071360' : '3852744'}</phone>"
        bn << "<description>"
        if offer['Id'][0] == "s"
          bn << "
            Жилой комплекс «Северная долина» от «Главстрой-СПб» – это новый перспективный район Санкт-Петербурга. Здесь формируется среда, комфортная для жизни современного горожанина.

            Лучший комплекс 2014 г.
            Площадь территории - 270 га
            Жилая застройка – 2,7 млн кв. м
            Объекты обслуживания населения - 375 тыс. кв. м
            Бюджет проекта – 75 млрд руб.
            Количество жителей – 80 тысяч человек
            Помимо жилых домов застройщик «Главстрой-СПб» возводит детские садики, школы, спортивные объекты, организует игровые площадки и зоны отдыха, строит дороги, подземные и наземные паркинги.
"
        else
          bn << "Новостройки «Юнтолово» возведены в экологически благоприятной зоне, недалеко от Юнтоловского заказника, Лахтинского разлива и Финского залива. Купив квартиру в ЖК «Юнтолово» в Приморском районе, вы получите все преимущества загородной жизни. При этом до ближайших станций метро «Старая деревня» и «Комендантский проспект» – около 15 минут на транспорте.
            Продажа квартир в ЖК «Юнтолово» в Приморском районе Санкт-Петербурга осуществляется по ценам застройщика. Вы можете выбрать удобную схему оплаты – внести всю сумму сразу, воспользоваться рассрочкой платежа от застройщика, купить квартиру при помощи ипотеки или одной из государственных программ. Покупателям предоставляются скидки."
        end
        bn << "</description>"
        bn << "<studio>+</studio>" if studia?(offer)
        bn << "<kkv_total>#{offer['Type'].to_i}</kkv_total>"
        bn << "<kkv>#{offer['Type'].to_i}</kkv>"
        bn << "<so>#{offer['Tsquare']}</so>"
        bn << "<sg>#{offer['Lsquare']}</sg>"
        bn << "<house_id>21</house_id>"
        bn << "<condition_id>2</condition_id>"
        bn << "<images>"
        bn << "<image>"
        bn << "<file_name_big>#{offer[:img]}</file_name_big>"
        bn << "<sort>0</sort>"
        bn << "</image>"
        bn << "</images>"
        bn << "</object>"

      end        
      bn << "</objects>"
      bn << '</root>'
      begin  
        File.open("public/bn.xml", 'w:windows-1251') { |file| file.puts bn.force_encoding("utf-8").encode("windows-1251", undef: :replace) }
      rescue
        sleep 10
        File.open("public/bn.xml", 'w:windows-1251') { |file| file.puts bn.force_encoding("utf-8").encode("windows-1251", undef: :replace) }
      end 
    end
    
    #AVITO%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    def write_xml_avito offers
      @noaddr = []
      avito = '<Ads formatVersion="3" target="Avito.ru">'
      offers.each do |offer|
        avito << "<Ad>#{@@nl}"
        avito << "<Id>#{offer['Id']}</Id>#{@@nl}"
        avito << "<Category>Квартиры</Category>#{@@nl}"
        avito << "<OperationType>Продам</OperationType>#{@@nl}"
        avito << "<Region>Санкт-Петербург</Region>#{@@nl}"
        avito << "<City>Санкт-Петербург</City>#{@@nl}"
        avito << "<Street>#{sevd?(offer) ? sev_addr(offer[:queue], offer[:build]) : yunt_addr(offer[:queue], offer[:build])}</Street>#{@@nl}"
        avito << "<Description><![CDATA["
        if offer['Id'][0] == "s"
          avito << "
<b>
Продается #{studia?(offer) ? 'Студия' : offer['Type'].to_i.to_s + 'к квартира' }
</b>
<br>
Общая площадь = #{offer['Tsquare'].to_i}
<br>
Этаж = #{offer[:floor].to_i}
<br>
<b>Срок сдачи в эксплуатацию</b>
<br>
#{offer[:plandate]}
<br>
<b>ЖК «Северная долина»</b>
<br><br>
<b> ЖК «Северная долина» </b> – один из крупнейших проектов комплексного освоения территорий в Санкт-Петербурге.  К настоящему моменту построены и введены в эксплуатацию 9 очередей строительства, площадью более 1,9 млн кв. м жилья. На этапе строительства – 10, 11, 12 очереди.
<br>
<br>
<b>Инфраструктура</b>
<br><br>
В комплексе уже построены 4 детских сада и 2 школы. В 2017 году завершится строительство пятого детского сада. К моменту завершения проекта в ЖК «Северная долина» будут функционировать 13 детских садов и 10 школ. Во дворах жилого комплекса организованы игровые площадки, зоны отдыха для взрослых и детей, предусмотрены подземные и наземные парковки.  <br>
На территории жилого комплекса открыты более 200 объектов  сферы услуг - это аптеки, отделение Сбербанка, продуктовые супермаркеты, фитнес-центр, кафе и пекарни, центры детского развития, салоны красоты, в непосредственной близости располагаются крупные торговые и досуговые центры –  ТК «Мега-Парнас» , ТРК «Гранд-Каньон».
<br><br>

<b>Транспортная доступность</b>
<br><br>
Ближайшая станция метро «Парнас» расположена в десятиминутной шаговой доступности от первых корпусов комплекса. Близость ключевых транспортных артерий – проспекта Энгельса и Выборгского шоссе, а также удобный выезд на КАД позволяет легко добраться до центра города и в ближайшие пригороды Санкт-Петербурга.
 <br><br>

<b>Условия покупки</b>
<br><br>

Приобрести квартиру в одном из комплексов, возводимых «Главстрой-СПб», можно с помощью различных способов оплаты: ипотечное кредитование от ведущих российских банков, удобная рассрочка от застройщика, а также реализовать социальные выплаты и субсидии в рамках государственных программ, таких как: «Военная ипотека», «Молодежи — доступное жилье», «Развитие долгосрочного жилищного кредитования в Санкт-Петербурге», «Материнский капитал».<br><br>
 
<b>Звоните, и менеджеры компании помогут вам с выбором квартиры и вариантом ее оплаты! </b> 
"
        else  
          avito << "
<b>
Продается #{studia?(offer) ? 'Студия' : offer['Type'].to_i.to_s + 'к квартира' }
</b>
<br>
Общая площадь =  #{offer['Tsquare'].to_i}
<br>
Этаж = #{offer[:floor].to_i}
<br>
<b>Срок сдачи в эксплуатацию</b>
<br>
#{offer[:plandate]}
<br>
<b>ЖК «Юнтолово»</b>
<br><br>

Новый жилой район «Юнтолово» возводится в экологически благоприятной зоне, недалеко от Юнтоловского заказника и Лахтинского разлива, Приморского района Санкт-Петербурга.
Преимуществами проекта являются – малоэтажное строительство и низкая плотность застройки. Все квартиры продаются с отделкой «под ключ»: полноценная жизнь в квартире с момента заселения.<br><br>
<b>Инфраструктура</b>
<br><br>
Компания «Главстрой-СПб» уделяет большое внимание развитию инфраструктуры и рекреационных зон в проекте «Юнтолово». На данный момент уже сдан детский сад на 140 мест. Активными темпами продолжается возведение общеобразовательной школы на 825 мест. <br><br>
Всего проектом планировки территории предусмотрено строительство 18 детских садов и 9 школ.
<br><br>
В жилом комплексе будет сформировано комфортное придомовое пространство с игровыми и спортивными площадками, зонами для активного и спокойного отдыха. 
<br><br>
<b>Транспортная доступность:</b>
<br><br>
Хорошая транспортная доступность: недалеко расположены съезды с КАД и ЗСД, Приморское шоссе. В 10 минутах езды на автомобиле – Финский залив. До ближайших станций метро «Старая деревня» и «Комендантский проспект» – около 15 минут на транспорте. <br><br>

<b>Условия покупки</b>
<br><br>

Приобрести квартиру в одном из комплексов, возводимых «Главстрой-СПб», можно с помощью различных способов оплаты: ипотечное кредитование от ведущих российских банков, удобная рассрочка от застройщика, а также реализовать социальные выплаты и субсидии в рамках государственных программ, таких как: «Военная ипотека», «Молодежи — доступное жилье», «Развитие долгосрочного жилищного кредитования в Санкт-Петербурге», «Материнский капитал».<br><br>

<b>Звоните, и менеджеры компании помогут вам с выбором квартиры и вариантом ее оплаты! </b>
"
        end
        avito << "]]></Description>#{@@nl}"
        avito << "<DateBegin>#{Time.now.strftime("%Y-%m-%d")}</DateBegin>#{@@nl}"
        avito << "<Price>#{offer['Dvalue'].to_i}</Price>#{@@nl}"
        avito << "<CompanyName>ООО Главстрой-СПб</CompanyName>#{@@nl}"
        avito << "<ContactPhone>#{sevd?(offer) ? '+7 812 245-33-67' : '+7 812 385-06-38'}</ContactPhone>#{@@nl}"
        avito << "<Images>#{@@nl}" 
        avito << "<Image url='#{sevd?(offer) ? @@sd_photos.shuffle.sample : @@y_photos.shuffle.sample}' />#{@@nl}"
        avito << "<Image url='#{offer[:img]}' />#{@@nl}"
        avito << "<Image url='#{offer['Id'][0] == "s" ? 'http://www.imageup.ru/img202/2610405/img_4723.jpg' : 'http://www.imageup.ru/img202/2610404/img_4888.jpg'}' />#{@@nl}"
        avito << "</Images>#{@@nl}"
        avito << "<Rooms>#{studia?(offer) ? 'Студия' : offer['Type'].to_i}</Rooms>#{@@nl}"
        avito << "<Square>#{offer['Tsquare'].to_i}</Square>#{@@nl}"
        avito << "<Floor>#{offer[:floor].to_i}</Floor>#{@@nl}"
        avito << "<Floors>#{sevd?(offer) ? '27' : '14'}</Floors>#{@@nl}"
        avito << "<Subway>#{sevd?(offer) ? 'Парнас' : 'Старая деревня'}</Subway>#{@@nl}"
        avito << "<HouseType>Монолитный</HouseType>#{@@nl}"
        avito << "<MarketType>Новостройка</MarketType>#{@@nl}"
        dev_id = sevd?(offer) ? sev_addr_id(offer[:queue], offer[:build]) : yunt_addr_id(offer[:queue], offer[:build])
        avito << "<NewDevelopmentId>#{dev_id}</NewDevelopmentId>#{@@nl}" unless dev_id.blank?
        @noaddr << (sevd?(offer) ? sev_addr(offer[:queue], offer[:build]) : yunt_addr(offer[:queue], offer[:build])) unless dev_id
        avito << "</Ad>#{@@nl}"
      end        
      avito << '</Ads>'
      
      File.open("public/avito_noaddr.txt", 'w'){ |file|  file.puts @noaddr.compact.uniq }
      begin  
        File.open("public/avito.xml", 'w'){ |file|  file.puts avito }
      rescue
        sleep 10
        File.open("public/avito.xml", 'w') { |file|  file.puts avito }
      end 
    end

  end  
end

