class CreateTables < ActiveRecord::Migration
  def change
    create_table :imgs, force: true do |t|
      t.string :offer
      t.string :url
      t.timestamps null: false
    end
    add_index :imgs, :offer

  end
end