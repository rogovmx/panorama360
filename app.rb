#require 'sqlite3'
require 'logger'
require 'open-uri'
require 'nokogiri'
require 'ap'
require "net/https"
require "uri"
require "json"
require "crack"
require 'rest-client'
require 'ramaze'
require 'rubygems'
require 'active_support/core_ext/numeric/time'
require 'active_record'
#require 'sqlite3'

#ActiveRecord::Base.logger = Logger.new('log/active_record.log')
#config = YAML::load File.read('config/database.yml')
#ActiveRecord::Base.establish_connection config

Dir["./lib/*.rb"].each {|file| require file }
Dir["./models/*.rb"].each {|file| require file }

#
#class Panorama < Ramaze::Controller
#  map '/panorama.yrl'
#
#  def index
#    parser = Feeder::Parser.new ip: request.ip
#    Thread.new { parser.perform_panorama }
#    return parser.get_file 'panorama_ok.xml'
#  end
#  
#end

#class Yuntolovo < Ramaze::Controller
#  map '/yuntolovo.yrl'
#
#  def index
#    parser = Feeder::Parser.new ip: request.ip
#    Thread.new { parser.perform_yuntolovo }
#    return parser.get_file 'yuntolovo_ok.xml'
#  end
#  
#end
#
#class Sevdol < Ramaze::Controller
#  map '/sevdol.yrl'
#
#  def index
#    parser = Feeder::Parser.new ip: request.ip
#    Thread.new { parser.perform_sevdol }
#    return parser.get_file 'sevdol_ok.xml'
#  end
#   
#end

class All < Ramaze::Controller
  map '/all.yrl'

  def index
    parser = Feeder::Parser.new ip: request.ip
    Thread.new { parser.perform_all }
    return parser.get_file 'all_ok.xml'
  end
   
end

class Bn < Ramaze::Controller
  map '/bn_2.xml'

  def index
    parser = Feeder::Parser.new ip: request.ip
    return parser.get_file 'all_ok.xml'
  end
   
end

#
#class Emls < Ramaze::Controller
#  map '/emls.xml'
#
#  def index
#    parser = Feeder::Parser.new ip: request.ip
#    parser.perform_emls
#    return parser.get_file 'emls_ok.xml'
#  end
#   
#end
#
#class Domo < Ramaze::Controller
#  map '/domofond.xml'
#
#  def index
#    parser = Feeder::Parser.new ip: request.ip
#    return parser.get_file 'all_ok_domo.xml'
#  end
#   
#end




#Ramaze.start
